#include <bool.h>
#include "window.h"
#include "keyboard.h"
#include "objects.h"

int main(int argc, char *argv[]){
	Window *win = createWindow(800, 608, "SDL");
	Keyboard key = {false};

	int sprBlock = loadImage(win, "resources/images/sprite.png");
	int sprPlayer = loadImage(win, "resources/images/touhou.png");

	Player player = {sprPlayer, 120, 20, 5};

	AllBlocks *blocks = initBlocks(sprBlock, 10);
	newBlock(blocks, 10, 10);
	newBlock(blocks, 42, 10);
	newBlock(blocks, 10, 42);

	bool close = false;
	SDL_Event e;
	while(!close){
		while(SDL_PollEvent(&e)){
			if(e.type == SDL_QUIT || (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_ESCAPE)){
				close = true;
			}
			getKey(&key.up, e, SDLK_UP);
			getKey(&key.down, e, SDLK_DOWN);
			getKey(&key.left, e, SDLK_LEFT);
			getKey(&key.right, e, SDLK_RIGHT);
		}
		while(SDL_GetTicks()-win->pTime < 1000.0/win->FPS);
		screenClear(win);
		//
		drawAllBlocks(win, blocks);
		player.x += player.spd*(key.right-key.left);
		player.y += player.spd*(key.down-key.up);
		drawImage(win, player.sprite, player.x, player.y);
		//
		SDL_Flip(win->screen);
		win->pTime = SDL_GetTicks();
	}
	freeBlocks(blocks);
	endSDL(win);
	return EXIT_SUCCESS;
}
