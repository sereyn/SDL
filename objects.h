#ifndef OBJECTS
#define OBJECTS

#include <bool.h>
#include "window.h"

typedef struct Player{
	int sprite;
	int x;
	int y;
	int spd;
} Player;

typedef struct Block{
	int sprite;
	int x;
	int y;
} Block;

typedef struct AllBlocks{
	Block *b;
	int sprite;
	int max;
	int num;
} AllBlocks;

AllBlocks *initBlocks(int, int);

void newBlock(AllBlocks*, int, int);

void drawAllBlocks(Window*, AllBlocks*);

void freeBlocks(AllBlocks *a);

#endif //OBJECTS