#ifndef COLOR
#define COLOR

typedef struct RGB{
	int red;
	int green;
	int blue;
} RGB;

RGB hexToRGB(int hex);

#endif //COLOR