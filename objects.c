#include "objects.h"

AllBlocks *initBlocks(int sprite, int max){
	AllBlocks *a = malloc(sizeof(*a));
	a->b = malloc(max*sizeof(Block));
	a->sprite = sprite;
	a->max = max;
	a->num = 0;
	return a;
}

void newBlock(AllBlocks *a, int x, int y){
	Block new = {a->sprite, x, y};
	a->b[a->num++] = new;
}

void drawAllBlocks(Window *w, AllBlocks *a){
	int i = 0;
	for(; i < a->num; i++){
		drawImage(w, a->b[i].sprite, a->b[i].x, a->b[i].y);
	}
}

void freeBlocks(AllBlocks *a){
	int i = 0;
	for(; i < a->num; i++){
		free(a->b+i);
	}
	free(a);
}
