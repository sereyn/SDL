#include "style.h"

void setColor(Window *w, const int hex){
	RGB col = hexToRGB(hex);
	w->color = SDL_MapRGB(w->screen->format, col.red, col.green, col.blue);
}

void setAlpha(Window *w, const double alpha){
	w->alpha = alpha;
}
