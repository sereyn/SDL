#include "images.h"

int loadImage(Window *w, const char *file){
	SDL_Surface *img = IMG_Load(file);
	if(img == NULL){
		SDL_ERROR()
	}
	w->allImages.image[w->allImages.loaded] = img;
	return w->allImages.loaded++;
}

void drawImage(Window *w, const int img, const int x, const int y){
	SDL_Rect p = {x, y};
	SDL_SetAlpha(w->allImages.image[img], SDL_SRCALPHA, w->alpha*0xFF);
	SDL_BlitSurface(w->allImages.image[img], NULL, w->screen, &p);
}
