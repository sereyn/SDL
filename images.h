#ifndef IMAGES
#define IMAGES

#include "window.h"

int loadImage(Window*, const char*);

void drawImage(Window*, const int, const int, const int);

#endif //IMAGES