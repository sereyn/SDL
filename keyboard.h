#ifndef KEYBOARD
#define KEYBOARD

#include <bool.h>
#include <SDL_image.h>

typedef struct Keyboard{
	bool up;
	bool left;
	bool down;
	bool right;
} Keyboard;

void getKey(bool*, const SDL_Event, const SDLKey);

#endif //KEYBOARD