#include "window.h"

void startSDL(void){
	if(SDL_Init(SDL_INIT_VIDEO) == -1){
		SDL_ERROR()
	}
}

void endSDL(Window *w){
	int i = 0;
	for(; i < w->allImages.loaded; i++){
		SDL_FreeSurface(w->allImages.image[i]);
	}
	free(w->allImages.image);
	free(w);
	SDL_Quit();
}

Window *createWindow(const int width, const int height, const char *title){
	startSDL();
	Window *w = malloc(sizeof(*w));
	w->width = width;
	w->height = height;
	w->FPS = 40;
	w->pTime = 0;
	w->screen = SDL_SetVideoMode(w->width, w->height, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);
	initializeImages(w, 50);
	if(w->screen == NULL){
		SDL_ERROR()
	}
	screenClear(w);
	SDL_WM_SetCaption(title, NULL);
	setIcon("resources/images/icon.png");
	SDL_EnableKeyRepeat(10, 10);
	return w;
}

void initializeImages(Window *w, int max){
	w->allImages.maxNumber = max;
	w->allImages.loaded = 0;
	w->allImages.image = malloc(sizeof(SDL_Surface)*w->allImages.maxNumber);
}

void setIcon(const char *file){
	SDL_Surface *img = IMG_Load(file);
	SDL_WM_SetIcon(img, NULL);
	SDL_FreeSurface(img);
}

void screenClear(Window *w){
	setAlpha(w, 1);
	setColor(w, 0xFFF);
	SDL_FillRect(w->screen, NULL, w->color);
}

void drawRect(Window *w, const int x, const int y, const int width, const int height){
	SDL_Rect p;
	p.x = x;
	p.y = y;
	SDL_Surface *r = SDL_CreateRGBSurface(SDL_HWSURFACE, width, height, 32, 0, 0, 0, 0);
	if(r == NULL){
		SDL_ERROR()
	}
	SDL_SetAlpha(r, SDL_SRCALPHA, w->alpha*255);
	SDL_FillRect(r, NULL, w->color); 
	SDL_BlitSurface(r, NULL, w->screen, &p);
	SDL_FreeSurface(r);
}
