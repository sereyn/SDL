#ifndef WINDOW
#define WINDOW

#include <SDL_image.h>
#include <bool.h>
#include "color.h"

#define SDL_ERROR()	fprintf(stderr, "Error: %s\n", SDL_GetError()); \
					exit(EXIT_FAILURE);

typedef struct Images{
	int maxNumber;
	int loaded;
	SDL_Surface **image;
} Images;

typedef struct Window{
	SDL_Surface *screen;
	Uint32 color;
	int width;
	int height;
	int FPS;
	unsigned int pTime;
	double alpha;
	Images allImages;
} Window;

#include "style.h"
#include "images.h"

void startSDL(void);

void endSDL(Window*);

Window *createWindow(const int, const int, const char*);

void initializeImages(Window*, int);

void setIcon(const char*);

void screenClear(Window *w);

void drawRect(Window*, const int, const int, const int, const int);

#endif //WINDOW
