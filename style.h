#ifndef STYLE
#define STYLE

#include "window.h"

void setColor(Window*, const int);

void setAlpha(Window*, const double);

#endif //STYLE